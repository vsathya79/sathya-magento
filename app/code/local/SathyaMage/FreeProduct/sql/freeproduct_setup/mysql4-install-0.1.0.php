<?php
/**
 * @author Sathya
 * @copyright Copyright (c) 2018
 * @package SathyaMage_FreeProduct
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('freeproduct_history')};
CREATE TABLE {$this->getTable('freeproduct_history')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `customer_email` varchar(255),
  `sku` varchar(255),
  `order_id` int(11),
  PRIMARY KEY ( `id` )
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE  {$this->getTable('freeproduct_history')} ADD UNIQUE  `ix_customer_email_sku` (  `customer_email` ,  `sku` )
");

$installer->endSetup();
?>