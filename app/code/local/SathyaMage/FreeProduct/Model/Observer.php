<?php
/**
 * @author Sathya
 * @copyright Copyright (c) 2018
 * @package SathyaMage_FreeProduct
 */
class SathyaMage_FreeProduct_Model_Observer
{
	public function addProductToOrder(Varien_Event_Observer $observer)
	{
		$orderIds = $observer->getData('order_ids');
		foreach($orderIds as $_orderId){
			$order = Mage::getModel('sales/order')->load($_orderId);
			try {
				$productStatus = Mage::getStoreConfig('freeproduct/productsettings/status');
				$productSku = Mage::getStoreConfig('freeproduct/productsettings/product_sku');
				if ($productStatus == 1 && $productSku != '' && $this->checkCustomer($order, $productSku) == 0) {
					$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
					$qty = 1;
					if ($product) {
						$this->saveProduct($order, $product);
					}
				}
			} catch (Exception $e) {
				Mage::logException($e);
			}
			return $this;
		}
	}

	private function checkCustomer($order, $productSku) {
		$collection = Mage::getModel('freeproduct/history')->getCollection();
		$collection->addFilter('customer_email', $order->getCustomerEmail());
		$collection->addFilter('sku', $productSku);

		return $collection->getSize();
	}

	private function saveProduct($order, $product) {
		$rowTotal = $product->getPrice();
		$orderItem = Mage::getModel('sales/order_item')
				->setStoreId($order->getStore()->getStoreId())
				->setQuoteItemId(NULL)
				->setQuoteParentItemId(NULL)
				->setProductId($product->getId())
				->setProductType($product->getTypeId())
				->setQtyBackordered(NULL)
				->setTotalQtyOrdered($qty)
				->setQtyOrdered($qty)
				->setName($product->getName())
				->setSku($product->getSku())
				->setPrice($product->getPrice())
				->setBasePrice($product->getPrice())
				->setOriginalPrice($product->getPrice())
				->setRowTotal($rowTotal)
				->setBaseRowTotal($rowTotal)
				->setOrder($order);
		$orderItem->save();
		$this->saveHistory($order, $product);
		
	}

	private function saveHistory($order, $product) {
		$history = Mage::getModel('freeproduct/history');
		$history->setData('customer_email', $order->getCustomerEmail());
		$history->setData('order_id', $order->getId());
		$history->setData('sku', $product->getSku());
		$history->save();
	}
}
