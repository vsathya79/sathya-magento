<?php
/**
 * @author Sathya
 * @copyright Copyright (c) 2018
 * @package SathyaMage_FreeProduct
 */
class SathyaMage_FreeProduct_Model_Mysql4_History extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("freeproduct/history", "id");
    }
}
?>