<?php
/**
 * @author Sathya
 * @copyright Copyright (c) 2018
 * @package SathyaMage_FreeProduct
 */
class SathyaMage_FreeProduct_Model_Mysql4_History_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

	public function _construct(){
		$this->_init("freeproduct/history");
	}
}
?>