<?php
/**
 * @author Sathya
 * @copyright Copyright (c) 2018
 * @package SathyaMage_FreeProduct
 */
class SathyaMage_FreeProduct_Model_History extends Mage_Core_Model_Abstract
{
    protected function _construct(){

       $this->_init("freeproduct/history");

    }

}
?>